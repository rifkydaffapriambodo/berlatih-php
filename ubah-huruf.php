<?php

function ubah_huruf($string) {
    //kode di sini
    for ($i = 0; $i < strlen($string); $i++) {
        $huruf = substr($string, $i, 1);
        if ($huruf == 'z') {
            $string = substr_replace($string, 'a', $i, 1);
        } else {
            $string = substr_replace($string, ++$huruf, $i, 1);
        }
    }
    return $string;
}

// TEST CASES
echo ubah_huruf('wow') . "<br>"; // xpx
echo ubah_huruf('developer') . "<br>"; // efwfmpqfs
echo ubah_huruf('laravel') . "<br>"; // mbsbwfm
echo ubah_huruf('keren') . "<br>"; // lfsfo
echo ubah_huruf('semangat') . "<br>"; // tfnbohbu
?>